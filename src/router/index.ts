import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Documents from "../views/Documents.vue";
import Document from "../views/Document.vue";
import AddDocument from "../views/AddDocument.vue";
import AddPage from "../views/AddPage.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Documents",
    component: Documents,
  },
  {
    path: "/",
    name: "AddDocument",
    component: AddDocument,
  },
  {
    path: "/:id",
    name: "Document",
    component: Document,
  },
  {
    path: "/:id",
    name: "AddPage",
    component: AddPage,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
