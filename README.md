# elevait_test

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Starting server to provide the database
```
node server/server.ts 
```
### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
