const { ObjectId, MongoClient } = require("mongodb");

const uri = "mongodb://doc_viewer:BJJjtTPyUV6XPd7x@94.130.203.236:27022/doc_viewer?authSource=doc_viewer&directConnection=true&maxPoolSize=100";
const client = new MongoClient(uri);

/*
Connect the server to the database
*/
async function connect() {
    try {
        await client.connect();
    } catch (e) {
        console.error(e);
    }
}

/*
get a all documents from the database
 */
async function getDocuments() {
    const docs = await client.db("doc_viewer").collection("documents").find().toArray();
    return docs;
}
/*
get the page from the database specified by the id
 */
async function getPage(id) {
    let page;
    try {
        page = await client.db("doc_viewer").collection("pages").find({ _id: new ObjectId(id) }).next();
    } catch (e) {
        console.error(e);
    }
    finally {
        return page;
    }

}
/*
get the document from the database specified by the id
 */
async function getDocument(id) {
    let doc;
    try {
        doc = await client.db("doc_viewer").collection("documents").find({ _id: new ObjectId(id) }).next();
    } catch (e) {
        console.error(e);
    }
    finally {
        return doc;
    }
}
/*
get the pages from the document specified by the id
 */
async function getPages(id) {
    let document = await getDocument(id);
    let pages = []
    try {
        for (let id of document.pages) {
            let page = await client.db("doc_viewer").collection("pages").find({ _id: id }).next();
            if (page) pages.push(page);
        }
    } catch (e) {
        console.error(e)
    } finally {
        return pages;
    }
}
/*
create and insert a document entry based on the author and title given by the data object
*/
async function createDocument(data) {
    //Das sollte eigentlich Typescript machen 
    if (typeof (data) !== "object" || data.author === undefined || data.title === undefined) {
        throw Error("ceateDocument requires an object with author and title as parameter")
    }
    data.type = "Document";
    data.dateCreated = new Date()
    data.pages = []
    await client.db("doc_viewer").collection("documents").insertOne(data)
}

/*
create and insert a page entry bases on the author and title given by the data object and link the document given by doc_id to the page
*/
async function createPage(doc_id, data) {
    //pagenr sollte int sein
    //das ist eigentlich redundant, aber spart error catching der id
    let document = await getDocument(doc_id);

    //Das sollte eigentlich Typescript machen 
    if (typeof (data) !== "object" || data.text === undefined || data.pageNr === undefined) {
        throw Error("ceateDocument requires an object with text and pageNr as parameter")
    }
    data.type = "DocumentPage";
    let res = await client.db("doc_viewer").collection("pages").insertOne(data)
    let id = res.insertedId;
    await client.db("doc_viewer").collection("documents").updateOne({ _id: document._id }, { $push: { pages: id } })
}

exports.createPage = createPage
exports.createDocument = createDocument
exports.getPages = getPages
exports.getPage = getPage
exports.getDocument = getDocument
exports.getDocuments = getDocuments
exports.connect = connect;


// async function main() {
//     await connect()
//     createPage("621cbead12deca6ffe7cf29e", {
//         text: "My Page", pageNr: 1
//     });
// }
// main()




