const database = require("./database.ts");

const express = require('express');
const cors = require('cors');
const app = express();
const port = 3000

app.use(express.json());
app.use(cors({
    origin: ['http://192.168.0.100:8080', 'http://localhost:8080']
}));

async function main() {
    await database.connect()

    app.get('/documents', async (req, res) => {

        res.send(await database.getDocuments())
    })

    app.get('/page/:id', async (req, res) => {
        let page = await database.getPage(req.params.id);
        res.send(page);
    })

    app.get('/document/:id', async (req, res) => {
        let pages = await database.getDocument(req.params.id);
        res.send(pages);
    })

    app.get('/pages/:id', async (req, res) => {
        let pages = await database.getPages(req.params.id);
        res.send(pages);
    })

    app.post('/createPage', async (req, res) => {
        try {
            await database.createPage(req.body.id, req.body.data)
        } catch (e) {
            console.error(e)
            res.send({ success: false })
        }
        res.send({ success: true })
    })

    app.post('/createDocument', async (req, res) => {
        try {
            await database.createDocument(req.body.data)
        } catch (e) {
            console.error(e)
            res.send({ success: false })
        }
        res.send({ success: true })
    })

    app.listen(port, () => {
        console.log(`listening on port ${port}`)
    })
}
main()
